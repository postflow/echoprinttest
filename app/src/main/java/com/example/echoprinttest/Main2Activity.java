package com.example.echoprinttest;

import android.media.MediaRecorder;
import android.os.ParcelFileDescriptor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.ShortBuffer;

import edu.gvsu.masl.echoprint.Codegen;

public class Main2Activity extends AppCompatActivity {


    MediaRecorder recorder;

    ByteArrayOutputStream byteArrayOutputStream;
    ParcelFileDescriptor[] descriptors;
    InputStream inputStream;
    ParcelFileDescriptor parcelRead;
    ParcelFileDescriptor parcelWrite;
    final byte[] data = new byte[300000];

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        Button buttonRec = findViewById(R.id.rec_button);
        Button buttonStop = findViewById(R.id.stop);

        byteArrayOutputStream = new ByteArrayOutputStream();

        try {
            descriptors = ParcelFileDescriptor.createPipe();
        } catch (IOException e) {
            e.printStackTrace();
        }
        parcelRead = new ParcelFileDescriptor(descriptors[0]);
        parcelWrite = new ParcelFileDescriptor(descriptors[1]);

        inputStream = new ParcelFileDescriptor.AutoCloseInputStream(parcelRead);




        buttonRec.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                recorder = new MediaRecorder();
                recorder.setAudioSource(MediaRecorder.AudioSource.MIC);
                recorder.setOutputFormat(MediaRecorder.OutputFormat.AMR_NB);
                recorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
                recorder.setOutputFile(parcelWrite.getFileDescriptor());
                try {
                    recorder.prepare();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                recorder.start();





                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        int read;


                        try{
                            while ((read = inputStream.read(data, 0, data.length)) != -1) {
                                byteArrayOutputStream.write(data, 0, read);
                            }
                        }
                        catch (Exception e){
                            e.printStackTrace();
                        }


                        try {
                            byteArrayOutputStream.flush();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                    }
                }).start();



            }
        });


        buttonStop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                recorder.stop();
                recorder.reset();
                recorder.release();



                short[] shorts = new short[data.length];

                for (int index = 0; index < shorts.length; index++){
                    shorts[index] = (short) data[index];
                }


                byte[] generate = null;

                try{
                    Codegen codegen = new Codegen();
                    generate = codegen.generate(shorts, shorts.length);
                } catch (Exception e ){
                    e.printStackTrace();
                }


                if(generate !=null){
                    Log.e("TEST", "generate.length = " + generate.length );
                } else {
                    Log.e("TEST", "generate = " + generate);
                }



            }
        });


    }
}
